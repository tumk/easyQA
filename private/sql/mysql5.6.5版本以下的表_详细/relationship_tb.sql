CREATE TABLE `relationship_tb`(
   `user_id` INT UNSIGNED NOT NULL COMMENT '用户ID',
   `ruser_id` INT UNSIGNED NOT NULL COMMENT '关系用户ID',
   `remark` CHAR(32) NULL DEFAULT NULL COMMENT '备注名称',
   `group_id` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '分组id',
   `rtype` ENUM('1','2','3') NOT NULL COMMENT '关系类型,1=单向关注 2=互相关注 3=悄悄关注 4=黑名单',
   `add_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '关系时间',
   INDEX ruserId_index (`ruser_id`),
   UNIQUE ids1_uq (`user_id`, `ruser_id`),
   UNIQUE ids2_uq (`ruser_id`, `user_id`),
   INDEX groupId_index (`group_id`),
   INDEX addTime_index (`add_time`)
 )  ENGINE=MYISAM COMMENT='用户关系表' ROW_FORMAT=FIXED CHARSET=utf8;